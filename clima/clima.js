const axios = require('axios');


const getClima = async(lat, lng) => {

    const resp = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&appId=e47351909945ff5e9afb50e014cd9d32`)

    return resp.data.main.temp;

}



module.exports = {
    getClima
}