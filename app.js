const lugar = require('./lugar/lugar');
const clima = require('./clima/clima');

const argv = require('yargs').options({
    direccion: {
        alias: 'd',
        desc: 'Direccion de la ciudad donde se va obtener el clima',
        demand: true
    }
}).argv;


//argv.direccion

//lugar.getLugarLatLng(argv.direccion)
//  .then(console.log);


//clima.getClima(40.750000, -74.000000)
//  .then(console.log)
//.catch(console.log)

const getInfo = (direccion) => {

    try {

        const coordenadas = await lugar.getLugarLatLng(direccion);
        const temp = await clima.getClima(coordenadas.lat, coordenadas.lng);

        return `El clima de ${coordenadas} es de ${temp}`;
    } catch (e) {

        return `No se puedo determinar el clima de ${direccion}`;
    }


    //salida
    //El clima de XXX es de XX
    //No se pudo determinar el clima de XX
}


getInfo(argv.direccion)
    .then(console.log)
    .catch(console.log);